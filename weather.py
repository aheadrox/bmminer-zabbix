#!/usr/bin/env python

from pyzabbix import ZabbixMetric, ZabbixSender
import json
import sys
import urllib

if __name__ == '__main__':
    
    link = "http://api.openweathermap.org/data/2.5/weather?lat=55.72&lon=37.68&appid=b99d5a6934ed1dfe7cac069fb857dcb8&units=metric"
    sender = ZabbixSender('127.0.0.1')

    try:
        f = urllib.urlopen(link)
        myfile = f.read()
        weather_obj = json.loads(myfile)
        dt = weather_obj["dt"]
    except:
        print("0")
        sys.exit(1)

    metrics = []

    for k,v in weather_obj["main"].items():
        key = "main_" + k
        m = ZabbixMetric(sys.argv[1], key, v, dt)
        metrics.append(m)

    for k,v in weather_obj["wind"].items():
        key = "wind_" + k
        m = ZabbixMetric(sys.argv[1], key, v, dt)
        metrics.append(m)
    
    sender.send(metrics)
    print("1")
