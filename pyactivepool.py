#!/usr/bin/env python

import json
import socket
import sys
import time
# import os

class CgminerAPI(object):
    """ Cgminer RPC API wrapper. """

    def __init__(self, host='localhost', port=4028):
        self.data = {}
        self.host = host
        self.port = port

    def command(self, command, arg=None):
        """ Initialize a socket connection,
        send a command (a json encoded dict) and
        receive the response (and decode it).
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)

        try:
            sock.connect((self.host, self.port))
            payload = {"command": command}
            if arg is not None:
                # Parameter must be converted to basestring (no int)
                payload.update({'parameter': unicode(arg)})

            sock.send(json.dumps(payload))
            received = self._receive(sock)
        except Exception as e:
            return dict({'STATUS': [{'STATUS': 'error', 'description': unicode(e)}]})
        else:
            # the null byte makes json decoding unhappy
            # also add a comma on the output of the `stats` command by replacing '}{' with '},{'
            return json.loads(received[:-1].replace('}{', '},{'))
        finally:
            # sock.shutdown(socket.SHUT_RDWR)
            sock.close()

    def _receive(self, sock, size=4096):
        msg = ''
        while 1:
            chunk = sock.recv(size)
            if chunk:
                msg += chunk
            else:
                # end of message
                break
        return msg

    def __getattr__(self, attr):
        """ Allow us to make command calling methods.
        >>> cgminer = CgminerAPI()
        >>> cgminer.summary()
        """

        def out(arg=None):
            return self.command(attr, arg)

        return out


if __name__ == '__main__':
    # print(sys.argv[1])
    SG = CgminerAPI(host=sys.argv[1],port=int(sys.argv[2]))

    try:
      stats_obj = json.loads(json.dumps(SG.command(command="pools")))  
      # print (json.dumps(stats_obj["POOLS"]))
    except:
      sys.exit(1)

    timestamp = int(time.time())
    #print(timestamp)

    poolindex = 0

    for i,pool in enumerate(stats_obj["POOLS"]):
        try: LST = int(pool["Last Share Time"])
        except ValueError:
            splited = pool["Last Share Time"].split(":")
            pool["Last Share Time"] = timestamp - ((int(splited[0])*3600)+(int(splited[1])*60)+int(splited[2]))

        if int(pool["Last Share Time"]) > int(stats_obj["POOLS"][poolindex]["Last Share Time"]):
            poolindex = i
    
    print(json.dumps(stats_obj["POOLS"][poolindex]))