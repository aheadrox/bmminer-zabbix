#!/usr/bin/env python

import paramiko
import sys
import json

if __name__ == '__main__':
   #lastip = sys.argv[1]
   host = sys.argv[1]
   user = 'root'
   secret = 'admin'
   # worker = 'korolevok.%s' % (lastip)
   port = 22


   print("SSH connection to [%s]... " % (host))

   ssh_client=paramiko.SSHClient()
   ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   try:
       ssh_client.connect(hostname=host,username=user,password=secret)
   except:
       print("Connection timeout!")
       sys.exit(1)

   stdin, stdout, stderr = ssh_client.exec_command("cat /config/bmminer.conf")
   bmconf = json.loads(stdout.read())
   bmconf["api-groups"] = "A:stats:pools:devs:summary:version:check:restart"

   #bmconf["pools"][0]["url"] = "stratum+tcp://stratum.slushpool.com:3333"
   #bmconf["pools"][0]["user"] = "ipgrb.%s" % (sys.argv[2])
   #bmconf["pools"][1]["url"] = "stratum+tcp://stratum.antpool.com:3333"
   #bmconf["pools"][1]["user"] = "elektrik.%s" % (sys.argv[2])
   #bmconf["pools"][2]["url"] = ""
   #bmconf["pools"][2]["user"] = ""

   #bmconf["pools"][0]["url"] = "stratum+tcp://stratum.slushpool.com:3333"
   #bmconf["pools"][0]["user"] = "kukuev.%s" % (sys.argv[2])
   #bmconf["pools"][1]["url"] = "stratum+tcp://stratum.antpool.com:3333"
   #bmconf["pools"][1]["user"] = "kuku1.kuku%s" % (sys.argv[2])
   #bmconf["pools"][2]["url"] = ""
   #bmconf["pools"][2]["user"] = ""

   bmdump = json.dumps(bmconf, indent=4, sort_keys=True)
   stdin, stdout, stderr = ssh_client.exec_command("/etc/init.d/bmminer.sh stop")
   stdin, stdout, stderr = ssh_client.exec_command("echo '%s' > /config/bmminer.conf" % (bmdump))
   #stdin, stdout, stderr = ssh_client.exec_command("/etc/init.d/bmminer.sh start")
   stdin, stdout, stderr = ssh_client.exec_command("/sbin/reboot")
   print(bmdump)
   ssh_client.close()
   print("SSH close connection")

