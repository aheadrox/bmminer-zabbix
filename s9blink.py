#!/usr/bin/env python

import paramiko
import sys
import json

if __name__ == '__main__':
   #lastip = sys.argv[1]
   host = sys.argv[1]
   user = 'root'
   secret = 'admin'
   # worker = 'korolevok.%s' % (lastip)
   port = 22


   print("SSH connection to [%s]... " % (host))

   ssh_client=paramiko.SSHClient()
   ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   try:
       ssh_client.connect(hostname=host,username=user,password=secret)
   except:
       print("Connection error!")
       sys.exit(1)

   stdin, stdout, stderr = ssh_client.exec_command("/etc/init.d/bmminer.sh stop")
   stdin, stdout, stderr = ssh_client.exec_command("echo 1 > /sys/class/gpio/gpio37/value")
   stdin, stdout, stderr = ssh_client.exec_command("echo 0 > /sys/class/gpio/gpio38/value")
   print("Blink RED")
   ssh_client.close()
   print("SSH close connection")

