#!/usr/bin/env python

import paramiko
import sys
import json

if __name__ == '__main__':
   lastip = sys.argv[1]
   host = '10.112.255.%s' % (lastip)
   user = 'root'
   secret = 'admin'
   worker = 'korolevok.%s' % (lastip)
   port = 22
   print("Host IP: %s" % (host))
   bmconf = json.load(open('bmminer.conf'))

   for pool in bmconf["pools"]:
       pool["user"] = worker
   
   #with open('./temp/bmminer_%s.conf' % (lastip), 'w') as outfile:
   # json.dump(bmconf, outfile)
   bmdump = json.dumps(bmconf,indent=4, sort_keys=True)
   print (bmdump)
   print("SSH connection")

   ssh_client=paramiko.SSHClient()
   ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   ssh_client.connect(hostname=host,username=user,password=secret)
   # sftp = ssh_client.open_sftp()
   # sftp.put('./temp/bmminer_%s.conf' % (lastip), '/config/bmminer.conf')
   # sftp.close()
   stdin, stdout, stderr = ssh_client.exec_command("/etc/init.d/bmminer.sh stop")
   stdin, stdout, stderr = ssh_client.exec_command("echo '%s' > /config/bmminer.conf" % (bmdump))
   stdin, stdout, stderr = ssh_client.exec_command("/etc/init.d/bmminer.sh start")
   # stdin, stdout, stderr = ssh_client.exec_command("/etc/init.d/bmminer.sh stop")
   #print(stdout.read())    
   ssh_client.close()
